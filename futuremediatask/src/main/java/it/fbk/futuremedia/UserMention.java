package it.fbk.futuremedia;

import java.util.ArrayList;

public class UserMention {

    private long id;

    public UserMention() {
    }

    public UserMention(long id) {
        this.id = id;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
}
