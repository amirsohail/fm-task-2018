package it.fbk.futuremedia;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.LineIterator;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.util.*;


public class MainClass {

    static List<UserMention> userMentionList = new ArrayList<>();

    static HashMap<Long, String> record = new HashMap<>();

    static HashMap<Long, String> record2 = new HashMap<>();

    public static void main(String[] args) throws IOException {

        PrintStream out = new PrintStream(new FileOutputStream("your-file-name.txt"));
        System.setOut(out);
        Gson gson = new Gson();


        List<User> userList = myMethodName(args[0], args[1]);

        List<Long> id = new ArrayList<>();
        List<Long> id2 = new ArrayList<>();
        userList.forEach(item -> {
            id.add(item.getId());
            record.put(item.getId(), item.getName());
            record2.put(item.getId(), item.getScreenName());
        });

        userMentionList.forEach(item ->
                id2.add(item.getId()));

        Set<Long> set = new HashSet<Long>(id);

        Set<Long> set2 = new HashSet<Long>(id2);

        System.out.println("Name           " + "ID           " + "Screen Name           " + "#Tweets        " + "Mentions");
        for (Long userId : record.keySet()) {
            System.out.print(record.get(userId) + "         " + userId + "       " + record2.get(userId) + "      ");
            set.stream().filter(
                    item -> item.longValue() == userId).forEach(item -> {
                        System.out.print(Collections.frequency(id, item));
                    }
            );

            set2.stream().filter(
                    item -> (item.longValue() == userId)).forEach(item -> {
                        System.out.print("     " + Collections.frequency(id2, item));

                    }
            );

            if (!set2.contains(userId))
                System.out.print("     " + "0");


            System.out.println();
        }
    }


    //Method to collect the users into a list
    public static List<User> myMethodName(final String path, final String name) throws IOException {

        //gson library to parse the json
        Gson gson = new Gson();
        List<User> userList = new ArrayList<>();

        //lineiterator to read the file line by line
        LineIterator it = FileUtils.lineIterator(new File(path), "UTF-8");
        try {
            while (it.hasNext()) {
                String line = it.nextLine();
                // do something with line
                //System.out.println(line);
                JsonObject jsonObject = gson.fromJson(line, JsonObject.class);


                //we are parsing the JsonObject by user element
                JsonObject user = jsonObject.getAsJsonObject("user");

                JsonObject entities = jsonObject.getAsJsonObject("entities");
                JsonArray userMentionsArray = entities.getAsJsonArray("user_mentions");


                //we are parsing the element user_mentions to count the mentionds for the specified user
                for (JsonElement jsonElement : userMentionsArray) {

                    JsonObject usermentions = jsonElement.getAsJsonObject();

                    UserMention userMention = new UserMention();
                    userMention.setId(usermentions.get("id").getAsLong());
                    if (user.get("name").getAsString().toLowerCase().equals(name.toLowerCase())) {
                        if ((long) user.get("id").getAsLong() == userMention.getId()) {
                            //counter.put(userMention.getId(), user.get("name").getAsString().toLowerCase());
                            userMentionList.add(new UserMention(userMention.getId()));
                        }
                    }
                }
                //condition to check if the name of the user matches with the name passed as an argument from command line
                if (user.get("name").getAsString().toLowerCase().equals(name.toLowerCase())) {
                    User userModel = new User();
                    userModel.setId((long) user.get("id").getAsLong());
                    userModel.setName(user.get("name").getAsString());
                    userModel.setScreenName(user.get("screen_name").getAsString());
                    userList.add(userModel);
                }
            }


        } finally {
            LineIterator.closeQuietly(it);
        }
        return userList;

    }
}
